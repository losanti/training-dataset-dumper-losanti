#ifndef HdRMin_DECORATOR_ALG_HH
#define HdRMin_DECORATOR_ALG_HH

#include "AthenaBaseComps/AthReentrantAlgorithm.h"

#include "GaudiKernel/ToolHandle.h"
#include "AthContainers/AuxElement.h"
#include "StoreGate/WriteDecorHandleKey.h"
#include "StoreGate/ReadDecorHandleKey.h"

#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "AthContainers/AuxElement.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"





/*
   This is a copy of 
   DerivationFrameworkMCTruth/​TruthClassificationDecorator.h
   to avoid requiring full Athena to access the 
   DerivationFrameworkMCTruth package. 
   
   A cleaner workaround would be to move the 
   ​TruthClassificationDecorator class to the 
   MCTruthClassifier package.
*/

class HitdRMinDecoratorAlg :  public AthReentrantAlgorithm { 
public:
  
  /** Constructors */
  HitdRMinDecoratorAlg(const std::string& name, ISvcLocator *pSvcLocator);
  
  /** Main routines */
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext&) const override ;


private:


  SG::ReadHandleKey<xAOD::TrackParticleContainer>   m_tracks{this,"TrackParticles", "InDetTrackParticles", "Track Particles"};
  SG::ReadHandleKey<xAOD::VertexContainer>   m_Vertex{this,"Vertex", "PrimaryVertices", "Vertex"};

  SG::WriteDecorHandleKey< xAOD::TrackParticleContainer > m_tracks_phi { this, "TrackParticles_phi", m_tracks, "phi_track"};
  SG::WriteDecorHandleKey< xAOD::TrackParticleContainer > m_tracks_eta { this, "TrackParticles_phi", m_tracks, "eta_track"};
  
  SG::ReadHandleKey<xAOD::TrackMeasurementValidationContainer>   m_JetPixelCluster{this,"JetAssociatedPixelClusters", "JetAssociatedPixelClusters", "PixelClusters"};
  SG::ReadHandleKey<xAOD::TrackMeasurementValidationContainer>   m_JetSCTCluster{this,"JetAssociatedSCTClusters", "JetAssociatedSCTClusters", "SCTClusters"};

  SG::WriteDecorHandleKey< xAOD::TrackMeasurementValidationContainer > m_JetSCTClusterdR_closestTrackHit { this, "JetAssociatedSCTClustersdR_closestTrackHit", m_JetSCTCluster, "dR_closestTrackHit"};
  SG::WriteDecorHandleKey< xAOD::TrackMeasurementValidationContainer > m_JetPixelClusterdR_closestTrackHit { this, "JetAssociatedPixelClustersdR_closestTrackHit", m_JetPixelCluster, "dR_closestTrackHit"};

  SG::WriteDecorHandleKey< xAOD::TrackMeasurementValidationContainer > m_JetSCTClusterEta { this, "JetAssociatedSCTClustersEta", m_JetSCTCluster, "eta_hit"};
  SG::WriteDecorHandleKey< xAOD::TrackMeasurementValidationContainer > m_JetSCTClusterPhi { this, "JetAssociatedSCTClustersPhi", m_JetSCTCluster, "phi_hit"};

  SG::WriteDecorHandleKey< xAOD::TrackMeasurementValidationContainer > m_JetPixelClusterEta { this, "JetAssociatedPixelClustersEta", m_JetPixelCluster, "eta_hit"};
  SG::WriteDecorHandleKey< xAOD::TrackMeasurementValidationContainer > m_JetPixelClusterPhi { this, "JetAssociatedPixelClustersPhi", m_JetPixelCluster, "phi_hit"}; 


  template <typename T>
  using Acc = SG::AuxElement::ConstAccessor<T>;
  Acc<float> m_eta_hit;
  Acc<float> m_phi_hit;
  Acc<float> m_eta_track;
  Acc<float> m_phi_track;

};


#endif