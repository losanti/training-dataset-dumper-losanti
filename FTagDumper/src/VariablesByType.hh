#ifndef VARIABLES_BY_TYPE_HH
#define VARIABLES_BY_TYPE_HH

// no idea why the forward declare fails in analysis base :'(
#ifdef XAOD_STANDALONE
#include <nlohmann/json.hpp>
#else
#include <nlohmann/json_fwd.hpp>
#endif

#include <string>
#include <vector>

struct VariablesByType
{
  using list_t = std::vector<std::string>;
  list_t uchars;
  list_t chars;
  list_t uints;
  list_t ints;
  list_t halves;
  list_t floats;
  list_t doubles;
  list_t ints_as_halves;
};

struct VariablesByCompression
{
  using list_t = std::vector<std::string>;
  list_t customs;
  list_t compressed;
};

// use the nlohmann conversion standard here
void from_json(const nlohmann::ordered_json&, VariablesByType&);
void from_json(const nlohmann::ordered_json&, VariablesByCompression&);

#endif
