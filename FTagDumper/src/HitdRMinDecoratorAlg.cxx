#include "HitdRMinDecoratorAlg.h"

#include "StoreGate/WriteDecorHandle.h"
#include "StoreGate/ReadDecorHandle.h"

#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "MCTruthClassifier/IMCTruthClassifier.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "AthLinks/ElementLink.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/Vertex.h"
#include <math.h>

#include "TrackSelector.hh"
#include <TVector3.h>

HitdRMinDecoratorAlg::HitdRMinDecoratorAlg(
    const std::string &name, ISvcLocator *loc) : AthReentrantAlgorithm(name, loc),
                                                  m_eta_hit("eta_hit"),
                                                  m_phi_hit("phi_hit"), 
                                                  m_eta_track("eta_track"),
                                                  m_phi_track("phi_track")

{
}

StatusCode HitdRMinDecoratorAlg::initialize()
{
  ATH_MSG_DEBUG("Inizializing " << name() << "... ");
  ATH_CHECK(m_tracks.initialize());
  ATH_CHECK(m_JetPixelCluster.initialize());
  ATH_CHECK(m_JetSCTCluster.initialize());
  ATH_CHECK(m_Vertex.initialize());

  ATH_CHECK(m_tracks_phi.initialize());
  ATH_CHECK(m_tracks_eta.initialize());
  ATH_CHECK(m_JetSCTClusterdR_closestTrackHit.initialize());
  ATH_CHECK(m_JetSCTClusterPhi.initialize());
  ATH_CHECK(m_JetSCTClusterEta.initialize());
  ATH_CHECK(m_JetPixelClusterdR_closestTrackHit.initialize());
  ATH_CHECK(m_JetPixelClusterPhi.initialize());
  ATH_CHECK(m_JetPixelClusterEta.initialize());

  return StatusCode::SUCCESS;
}

StatusCode HitdRMinDecoratorAlg::execute(const EventContext &ctx) const
{
  ATH_MSG_DEBUG("Executing " << name() << "... ");

  SG::ReadHandle<xAOD::TrackParticleContainer> tracks{m_tracks, ctx};
  // SG::ReadHandle<xAOD::VertexConatainer>
  SG::ReadHandle<xAOD::TrackMeasurementValidationContainer> JetPixelCluster{m_JetPixelCluster, ctx};
  SG::ReadHandle<xAOD::TrackMeasurementValidationContainer> JetSCTCluster{m_JetSCTCluster, ctx};
  SG::ReadHandle<xAOD::VertexContainer>   Vertices{m_Vertex, ctx};

  SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, float> JetSCTClusterdR_closestTrackHit{m_JetSCTClusterdR_closestTrackHit, ctx};
  SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, float> JetSCTClusterPhi{m_JetSCTClusterPhi, ctx};
  SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, float> JetSCTClusterEta{m_JetSCTClusterEta, ctx};
  SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, float> JetPixelClusterdR_closestTrackHit{m_JetPixelClusterdR_closestTrackHit, ctx};
  SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, float> JetPixelClusterPhi{m_JetPixelClusterPhi, ctx};
  SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, float> JetPixelClusterEta{m_JetPixelClusterEta, ctx};
  SG::WriteDecorHandle<xAOD::TrackParticleContainer, float> tracks_phi{m_tracks_phi, ctx};
  SG::WriteDecorHandle<xAOD::TrackParticleContainer, float> tracks_eta{m_tracks_eta, ctx};

  if (!Vertices.isValid())
  {
    ATH_MSG_ERROR("Couldn't find Vertices");
    return StatusCode::FAILURE;
  }
  if (!JetPixelCluster.isValid())
  {
    ATH_MSG_ERROR("Couldn't find Pixel");
    return StatusCode::FAILURE;
  }
  if (!JetSCTCluster.isValid())
  {
    ATH_MSG_ERROR("Couldn't find SCT");
    return StatusCode::FAILURE;
  }
  if (!tracks.isValid())
  {
    ATH_MSG_ERROR("Couldn't find tracks");
    return StatusCode::FAILURE;
  }

  // loop over Hits and associate each hit, that was contained in a track reco, with the ID of the corresponding track
  std::vector<std::pair<SG::ReadHandle<xAOD::TrackMeasurementValidationContainer>, SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, float>>> pairVec_dR_TrackHit;
  pairVec_dR_TrackHit.emplace_back(std::move(JetPixelCluster), std::move(JetPixelClusterdR_closestTrackHit));
  pairVec_dR_TrackHit.emplace_back(std::move(JetSCTCluster), std::move(JetSCTClusterdR_closestTrackHit));

  std::vector<std::pair<SG::ReadHandle<xAOD::TrackMeasurementValidationContainer>, SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, float>>> pairVec_eta;
  pairVec_eta.emplace_back(std::move(JetPixelCluster), std::move(JetPixelClusterEta));
  pairVec_eta.emplace_back(std::move(JetSCTCluster), std::move(JetSCTClusterEta));

  std::vector<std::pair<SG::ReadHandle<xAOD::TrackMeasurementValidationContainer>, SG::WriteDecorHandle<xAOD::TrackMeasurementValidationContainer, float>>> pairVec_phi;
  pairVec_phi.emplace_back(std::move(JetPixelCluster), std::move(JetPixelClusterPhi));
  pairVec_phi.emplace_back(std::move(JetSCTCluster), std::move(JetSCTClusterPhi));

  int count = 0;
  TVector3 vx3(0,0,0);

  for (auto vx : *Vertices) 
  {
    if ( vx->vertexType() == xAOD::VxType::PriVtx ) 
    {
      if (count >= 1)
      {
        ATH_MSG_ERROR("More than one Primary Vertex");
        return StatusCode::FAILURE;
      }
      vx3.SetX(vx->x());
      vx3.SetY(vx->y());
      vx3.SetZ(vx->z());

      count++;
    }
  }


  for (auto iter : pairVec_eta)
  {
    for (const auto hit : *(iter.first))
    {
      TVector3 v_hit(hit->globalX(), hit->globalY(), hit->globalZ());
      TVector3 v_hit_cleaned = v_hit-vx3;
      iter.second(*hit) = v_hit_cleaned.Eta();
    }
  }
  for (auto iter : pairVec_phi)
  {
    for (const auto hit : *(iter.first))
    {
      TVector3 v_hit(hit->globalX(), hit->globalY(), hit->globalZ());
      TVector3 v_hit_cleaned = v_hit-vx3;
      iter.second(*hit) = v_hit_cleaned.Phi();
    }
  }

  for (auto track : *tracks)
  {
    tracks_phi(*track) = track->phi();
    tracks_eta(*track) = track->eta();
  }

  for (auto iter : pairVec_dR_TrackHit)
  {
    for (const auto hit : *(iter.first))
    { 
      float Phi_hit = m_phi_hit(*hit);
      float Eta_hit = m_eta_hit(*hit);
      float dR_2 = 4.;
      for (const auto track : *tracks)
      { 
        float Phi_track = m_phi_track(*track);
        float Eta_track = m_eta_track(*track);

        float deta = Eta_hit-Eta_track;
        float dphi= Phi_hit-Phi_track;

        if (dphi > M_PI) 
        {
             dphi -= M_PI*2;
        } else if (dphi <= -M_PI) 
        {
            dphi += M_PI*2;
        }

        float tmp_dR_2 = dphi*dphi+deta*deta;
        if (tmp_dR_2 < dR_2)
        {
          dR_2 = tmp_dR_2;
        }
      }
      iter.second(*hit) = std::sqrt(dR_2);
    }
  }
  return StatusCode::SUCCESS;
}
