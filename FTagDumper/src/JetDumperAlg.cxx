#include "JetDumperAlg.h"

#include "JetDumperTools.hh"
#include "JetDumperConfig.hh"
#include "processEvent.hh"
#include "ConfigFileTools.hh"

#include "H5Cpp.h"

#include "StoreGate/ReadHandle.h"

#include <nlohmann/json.hpp>
#include <filesystem>

JetDumperAlg::JetDumperAlg(const std::string& name,
                             ISvcLocator* pSvcLocator):
  AthAlgorithm(name, pSvcLocator),
  m_tools(nullptr),
  m_outputs(nullptr)
{
  declareProperty("configJson", m_config_json);
  declareProperty("metadataFileName",
                  m_metadata_file_name = "userJobMetadata.json");
}

JetDumperAlg::~JetDumperAlg() {
}

// these are the functions inherited from Algorithm
StatusCode JetDumperAlg::initialize () {
  auto nlocfg = nlohmann::ordered_json::parse(m_config_json);
  std::string prefix = name();
  if (!m_h5_dir.empty()) prefix = m_h5_dir;

  auto cfg = get_jetdumper_config(nlocfg, prefix);
  if (m_force_full_precision) force_full_precision(cfg);
  m_config.reset(new JetDumperConfig(cfg));
  m_tools.reset(new JetDumperTools(*m_config));

  ATH_CHECK(m_output_svc.retrieve());
  if (m_h5_dir.empty()) {
    m_outputs.reset(new JetDumperOutputs(*m_config, *m_output_svc->group()));
    addAttributeToHDF5(m_config_json, *m_output_svc->group());
  } else {
    H5::Group group(m_output_svc->group()->createGroup(m_h5_dir));
    m_outputs.reset(new JetDumperOutputs(*m_config, group));
    addAttributeToHDF5(m_config_json, group);
  }

  m_jetKey = cfg.jet_collection;
  ATH_CHECK(m_jetKey.initialize());
  ATH_MSG_DEBUG("Initialized " << name());

  return StatusCode::SUCCESS;
}
StatusCode JetDumperAlg::execute () {

  // dereference a few things for convenience
  auto& event = *evtStore();
  const auto& jobcfg = *m_config;
  auto& tools = *m_tools;
  auto& outputs = *m_outputs;

  // most of the real work happens in this function
  processEvent(event, jobcfg, tools, outputs);

  return StatusCode::SUCCESS;
}
StatusCode JetDumperAlg::finalize () {

  if (m_metadata_file_name.size() > 0) {
    writeJobMetadata(m_tools->timings, m_metadata_file_name);
  }

  ATH_MSG_INFO("Successful run.");
  return StatusCode::SUCCESS;
}
