
############################################
# Dijet and Z+jet samples (l-jet GN2 calibration - Vid Homsak)
############################################
# MC20 - Run 2
#mc20_13TeV.700325.Sh_2211_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG1.e8351_s3681_r13167_p5981
#mc20_13TeV.700325.Sh_2211_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG1.e8351_s3681_r13144_p5981
#mc20_13TeV.700325.Sh_2211_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG1.e8351_s3681_r13145_p5981
#mc20_13TeV.700322.Sh_2211_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG1.e8351_s3681_r13144_p5981
#mc20_13TeV.700322.Sh_2211_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG1.e8351_s3681_r13167_p5981
#mc20_13TeV.700322.Sh_2211_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG1.e8351_s3681_r13145_p5981

# MC23 - Run 3
#mc23_13p6TeV.700791.Sh_2214_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG1.e8514_s4162_r14622_p5981
#mc23_13p6TeV.700791.Sh_2214_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG1.e8514_s4159_r14799_p5981
#mc23_13p6TeV.700788.Sh_2214_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG1.e8514_s4162_r14622_p5981
#mc23_13p6TeV.700788.Sh_2214_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG1.e8514_s4159_r14799_p5981
#mc23_13p6TeV.601700.PhPy8EG_dijet_JZ1.deriv.DAOD_FTAG1.e8514_s4162_r14622_p5981
#mc23_13p6TeV.601701.PhPy8EG_dijet_JZ2.deriv.DAOD_FTAG1.e8514_s4162_r14622_p5981
#mc23_13p6TeV.601702.PhPy8EG_dijet_JZ3.deriv.DAOD_FTAG1.e8514_s4162_r14622_p5981
#mc23_13p6TeV.601703.PhPy8EG_dijet_JZ4.deriv.DAOD_FTAG1.e8514_s4162_r14622_p5981
#mc23_13p6TeV.601704.PhPy8EG_dijet_JZ5.deriv.DAOD_FTAG1.e8514_s4162_r14622_p5981
#mc23_13p6TeV.601705.PhPy8EG_dijet_JZ6.deriv.DAOD_FTAG1.e8514_s4162_r14622_p5981
mc23_13p6TeV.601706.PhPy8EG_dijet_JZ7.deriv.DAOD_FTAG1.e8514_s4162_r14622_p5981