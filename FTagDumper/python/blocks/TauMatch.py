from dataclasses import dataclass, field

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .BaseBlock import BaseBlock


@dataclass
class TauMatch(BaseBlock):
    """_summary_

    Args:
        BaseBlock (_type_): _description_
    """
    jet_collection: str = None
    floats: list[str] = field(default_factory=lambda: ['RNNJetScoreSigTrans'])
    ints: list[int] = field(default_factory=list)

    def __post_init__(self):
        if self.jet_collection is None:
            self.jet_collection = self.dumper_config["jet_collection"]

    def to_ca(self):
        ca = ComponentAccumulator()

        # Get the jet -> btag link
        jc = self.jet_collection.replace('Jets','')
        
        tc = 'TauJets'
        ca.addEventAlgo(
            CompFactory.BTagToJetLinkerAlg(
                f'jetToBTagFor{jc}',
                newLink=f'BTagging_{jc}.jetLink',
                oldLink=f'{jc}Jets.btaggingLink'
            )
        )
        # Match tau jet to jets
        ca.addEventAlgo(
            CompFactory.JetMatcherAlg(
                f'{tc}To{self.jet_collection}CopyAlg',
                targetJet=self.jet_collection,
                sourceJets=[tc],
                floatsToCopy={f:f for f in self.floats},
                intsToCopy={i:i for i in self.ints},
                dR='deltaRToTauJet',
                dPt='deltaPtToTauJet',
                ptPriorityWithDeltaR=0.3,
            )
        )
        return ca